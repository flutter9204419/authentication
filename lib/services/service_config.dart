import 'package:authentication/services/api_services.dart';
import 'package:authentication/services/base_service.dart';

mixin ConfigMixin {
  BaseServices? api;

  void setAppConfig() {
    api = ApiServices();
  }
}
