import 'package:authentication/style/dimensions.dart';
import 'package:authentication/style/text_style.dart';
import 'package:flutter/material.dart';

class CommonAppButton extends StatelessWidget {
  final Color backgroundColor;
  final String buttonText;
  final Function() onPressed;
  const CommonAppButton({
    super.key,
    required this.backgroundColor,
    required this.buttonText,
    required this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onPressed,
      style: ElevatedButton.styleFrom(
        backgroundColor: backgroundColor,
        elevation: 0,
        padding: const EdgeInsets.all(
          Dimensions.kPaddingSmallSize,
        ),
      ),
      child: Text(
        buttonText,
        style: kButtomTextStyle,
        // style: Tx,
      ),
    );
  }
}

class CommonTextButton extends StatelessWidget {
  final Function() onPressed;
  final Widget child;
  const CommonTextButton({
    super.key,
    required this.onPressed,
    required this.child,
  });

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: onPressed,
      style: TextButton.styleFrom(
        padding: EdgeInsets.zero,
        minimumSize: const Size(50, 20),
        tapTargetSize: MaterialTapTargetSize.shrinkWrap,
        // alignment: Alignment.centerLeft,
      ),
      child: child,
    );
  }
}
