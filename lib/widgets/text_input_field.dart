import 'package:authentication/style/dimensions.dart';
import 'package:authentication/style/text_style.dart';
import 'package:authentication/utils/color.dart';
import 'package:flutter/material.dart';

class TextInputField extends StatelessWidget {
  final TextEditingController? controller;
  final Widget? suffixIcon;
  final bool obscureText;
  final TextInputType keyboardType;
  final String hintText;
  final bool? enable;

  const TextInputField(
      {super.key,
      this.controller,
      this.suffixIcon,
      required this.keyboardType,
      required this.obscureText,
      required this.hintText,
      this.enable});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      enabled: enable,
      obscureText: obscureText,
      cursorColor: kAppColor,
      controller: controller,
      style: kInputTextStyle,
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        hintText: hintText,
        hintStyle: kHintTextStyle,
        contentPadding: const EdgeInsets.symmetric(
            vertical: 8, horizontal: Dimensions.kPaddingNormalSize),
        focusedBorder: kTextInputBorder,
        enabledBorder: kTextInputBorder,
        disabledBorder: kTextInputBorder,
        suffixIcon: suffixIcon,
      ),
    );
  }
}
