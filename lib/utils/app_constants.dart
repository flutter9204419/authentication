//Application Const Text for validation Message,App Name, App Const Text
class AppConstants {
  //Tearms condition url
  static const String kUrl = "https://www.google.com";

  // Name of the application
  static const String kAppName = "Authetication";

  //All Static text, UIlable and other static Text
  static const String kAppBarText = "AuthenticationDemo";
  static const String kCompleteProfile = "Complete Profile";
  static const String kSignIn = "Sign In";
  static const String kSignUp = "Sign Up";
  static const String kSignOut = "Sign Out";
  static const String kError = "Error";
  static const String kDontHaveAccountYet = "Don't have an account yet?";
  static const String kOrSignInWith = "Or sign in with";
  static const String kIAgreeTermCondition = "I agree to the";
  static const String kForgotPasswordSubtitle =
      "Please enter your registered email ID and we’ll send you a varification link to reset your password";
  static const String kForgotPassword = "Forgot Password";
  static const String kForgotPasswordQueMark = "Forgot Password?";
  static const String kVarificationMessageFirst =
      "We have sent varification mail on this mail id";
  static const String kVarificationMessageLast = "please verify it.";
  static const String kWelcomeMessageFirst =
      "Welcome to the Authentication Demo app";
  static const String kWelcomeMessageLast = "Thank you for sign in.";
  static const String kLogoutConfirmation = "Are you sure you want to logout?";
  static const String kEmailVarification = "Email varification";
  static const String kEmailVarificationSendMessage =
      "Email for reset password is sent successfully.";

  //Button Label
  static const String kLoginWithGoogle = "Google";
  static const String kLoginWithFacebook = "Facebook";
  static const String kOk = "Ok";
  static const String kTearmCondition = "Terms & Condition";
  static const String kSend = "Send";
  static const String kContinue = "Continue";
  static const String kCancel = "Cancel";
  static const String kProceed = "Proceed";

  // Validation Message
  static const String kPleaseEnterName = "Please enter name";
  static const String kPleaseEnterEmailId = "Please enter email id";
  static const String kPleaseEnterSocialLoginId =
      "Please enter social login id";
  static const String kPleaseEnterValidEmailId = "Please enter valid email id";
  static const String kPleaseEnterPassword = "Please enter password";
  static const String kPleaseEnterConfirmPaassword =
      "Please enter confirm password";
  static const String kConfirmPasswordShouldMatchWithPassword =
      "Confirm password should be same as a password";
  static const String kPleaseEnterEmailOrUsername =
      "Please enter email id or username";
  static const String kPleaseSelectTermsAndConditions =
      "Please select terms and condtions";

  //TextInput Hint Text label
  static const String kName = "Name";
  static const String kEmailUsername = "Email / Username";
  static const String kPassword = "Password";
  static const String kConfirmPassword = "Confirm Password";
  static const String kEmail = "Email id";
}
