import 'package:flutter/material.dart';

const Color kAppColor = Colors.black;
const Color kWhite = Colors.white;
const Color kFacebookColor = Color(0xFF3b579d);
const Color kGoogleColor = Color(0xFFdc4e41);
const Color kHintColor = Color(0xFFBDBDBD);
const Color kTextInputBorderColor = Color(0xFFBDBDBD);
const Color kLinkColor = Color(0xff0000EE);
