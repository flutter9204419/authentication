import 'package:authentication/style/dimensions.dart';
import 'package:authentication/style/text_style.dart';
import 'package:authentication/utils/app_constants.dart';
import 'package:authentication/utils/color.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Utility {
  // This method use for Error Sankbar
  static snackBar(String msg, BuildContext context) {
    var snackBar = SnackBar(
      behavior: SnackBarBehavior.floating,
      margin: const EdgeInsets.symmetric(
          horizontal: Dimensions.kPaddingExtraSize * 2,
          vertical: Dimensions.kPaddingExtraSize),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(Dimensions.kButtonRadius),
      ),
      content: Text(
        msg,
      ),
      elevation: 0,
      duration: const Duration(seconds: 2), // snakbar duration
    );

    //close previous all snakbar before new comes
    ScaffoldMessenger.of(context).clearSnackBars();
    //display sankbar using this method
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  static showDialog(String message, [Function()? onPressed, String? title]) {
    Get.defaultDialog(
      backgroundColor: kWhite,
      confirm: ElevatedButton(
          onPressed: onPressed ?? () => Get.back(),
          style: ElevatedButton.styleFrom(
            backgroundColor: kAppColor,
            elevation: 0,
            padding: const EdgeInsets.symmetric(horizontal: 30),
          ),
          child: const Text(
            AppConstants.kOk,
            style: kButtomTextStyle,
          )),
      title: title ?? "",
      titlePadding: EdgeInsets.only(top: title == null ? 0 : 15),
      titleStyle: const TextStyle(fontSize: 18, fontWeight: FontWeight.w700),
      content: Text(message,textAlign: TextAlign.center,),
      buttonColor: kAppColor,
    );
  }
}
