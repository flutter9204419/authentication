import 'package:authentication/style/dimensions.dart';
import 'package:authentication/style/text_style.dart';
import 'package:authentication/utils/app_constants.dart';
import 'package:authentication/utils/color.dart';
import 'package:authentication/views/forgot_password/forgot_controller.dart';
import 'package:authentication/widgets/common_app_button.dart';
import 'package:authentication/widgets/text_input_field.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ForgotView extends GetView<ForgotController> {
  const ForgotView({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder(
      init: ForgotController(),
      builder: (ForgotController controller) {
        return GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: SafeArea(
            child: Scaffold(
              appBar: AppBar(automaticallyImplyLeading: true),
              body: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.all(Dimensions.kPaddingExtraSize),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      // const SizedBox(height: 40),
                      const Text(AppConstants.kForgotPassword,
                          style: kAppLabelStyle),
                      const SizedBox(height: 20),
                      const Text(
                        AppConstants.kForgotPasswordSubtitle,
                        style: kForgotLabelStyle,
                      ),
                      const SizedBox(height: 30),
                      TextInputField(
                        obscureText: false,
                        hintText: AppConstants.kEmail,
                        keyboardType: TextInputType.emailAddress,
                        controller: controller.sendEmailController,
                      ),
                      const SizedBox(height: 30),
                      SizedBox(
                        width: double.infinity,
                        child: CommonAppButton(
                          backgroundColor: kAppColor,
                          buttonText: AppConstants.kSend,
                          onPressed: () {
                            controller.sendMail();
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
