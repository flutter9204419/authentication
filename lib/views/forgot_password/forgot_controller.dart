import 'package:authentication/controller/base_controller.dart';
import 'package:authentication/utils/app_constants.dart';
import 'package:authentication/utils/utilitys.dart';
import 'package:authentication/utils/validation_extension.dart';
import 'package:flutter/material.dart';

class ForgotController extends BaseController {
  TextEditingController sendEmailController = TextEditingController();

  void sendMail() {
    if (sendEmailController.text.isEmpty) {
      // Utility.snackBar(AppConstants.kPleaseEnterEmailId, Get.context!);
      Utility.showDialog(AppConstants.kPleaseEnterEmailId);
    } else if (sendEmailController.text.isValidEmail() == false) {
      // Utility.snackBar(AppConstants.kPleaseEnterValidEmailId, Get.context!);
      Utility.showDialog(AppConstants.kPleaseEnterValidEmailId);
    } else {
      sendEmailController.clear();
      // Utility.snackBar(
      //     AppConstants.kEmailVarificationSendMessage, Get.context!);
      Utility.showDialog(AppConstants.kEmailVarificationSendMessage);
    }
  }
}
