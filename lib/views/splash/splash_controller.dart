import 'package:authentication/controller/base_controller.dart';
import 'package:authentication/routes/app_routes.dart';
import 'package:authentication/style/dimensions.dart';
import 'package:get/get.dart';

class SplashController extends BaseController {
  @override
  void onInit() {
    getLoginDetails();
    super.onInit();
  }

  void getLoginDetails() {
    // Change the duration of your splash screen here
    Future.delayed(Duration(seconds: Dimensions.screenLoadTime), () {
      Get.offNamedUntil(Routes.login, (route) => false);
    });
  }
}
