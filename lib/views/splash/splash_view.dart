import 'package:authentication/style/dimensions.dart';
import 'package:authentication/views/splash/splash_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SplashView extends GetView<SplashController> {
  const SplashView({super.key});

  @override
  Widget build(BuildContext context) {
    Dimensions.screenWidth = MediaQuery.of(context).size.width;
    Dimensions.screenHeight = MediaQuery.of(context).size.height;
    return GetBuilder(
        init: SplashController(),
        builder: (SplashController controller) {
          return SafeArea(
              top: false,
              child: Scaffold(
                body: Center(
                    child: Image.asset(
                  "ImagePath.appLogo",
                  height: Dimensions.screenHeight / 2,
                )),
              ));
        });
  }
}
