import 'package:authentication/routes/app_routes.dart';
import 'package:authentication/style/dimensions.dart';
import 'package:authentication/style/text_style.dart';
import 'package:authentication/utils/app_constants.dart';
import 'package:authentication/utils/color.dart';
import 'package:authentication/views/login/login_controller.dart';
import 'package:authentication/widgets/common_app_button.dart';
import 'package:authentication/widgets/text_input_field.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LoginView extends GetView<LoginController> {
  const LoginView({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder(
      init: LoginController(),
      builder: (LoginController controller) {
        return GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: SafeArea(
            child: Scaffold(
              body: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.all(Dimensions.kPaddingExtraSize),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(height: 55),
                      const Text(
                        AppConstants.kSignIn,
                        style: kAppLabelStyle,
                      ),
                      const SizedBox(height: 10),
                      TextInputField(
                        obscureText: false,
                        hintText: AppConstants.kEmailUsername,
                        keyboardType: TextInputType.emailAddress,
                        controller: controller.emailController,
                      ),
                      const SizedBox(height: 15),
                      TextInputField(
                        obscureText: controller.passwordVisible,
                        hintText: AppConstants.kPassword,
                        keyboardType: TextInputType.name,
                        controller: controller.passwordController,
                        suffixIcon: IconButton(
                          onPressed: () {
                            controller.passwordVisible =
                                !controller.passwordVisible;
                            controller.update();
                          },
                          icon: Icon(
                            controller.passwordVisible
                                ? Icons.visibility
                                : Icons.visibility_off,
                            color: kAppColor,
                          ),
                        ),
                      ),
                      const SizedBox(height: 15),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          CommonTextButton(
                            onPressed: () {
                              Get.toNamed(Routes.forgot);
                            },
                            child: const Text(
                              AppConstants.kForgotPasswordQueMark,
                              style: kTextStyle14,
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(height: 30),
                      SizedBox(
                        width: double.infinity,
                        child: CommonAppButton(
                          backgroundColor: kAppColor,
                          buttonText: AppConstants.kSignIn,
                          onPressed: () {
                            controller.loginApi();
                          },
                        ),
                      ),
                      const SizedBox(height: 15),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Text(
                            "${AppConstants.kDontHaveAccountYet} ",
                            style: kTextStyle14,
                          ),
                          CommonTextButton(
                            onPressed: () {
                              Get.toNamed(Routes.register);
                            },
                            child: const Text(
                              AppConstants.kSignUp,
                              style: kLintButtonStyle,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              bottomNavigationBar: Padding(
                padding: const EdgeInsets.all(Dimensions.kPaddingExtraSize),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    const Text(AppConstants.kOrSignInWith),
                    const SizedBox(height: 10),
                    Row(
                      children: [
                        Expanded(
                          child: CommonAppButton(
                            backgroundColor: kFacebookColor,
                            buttonText: AppConstants.kLoginWithFacebook,
                            onPressed: () {
                              controller.facebookLogin();
                            },
                          ),
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: CommonAppButton(
                            backgroundColor: kGoogleColor,
                            buttonText: AppConstants.kLoginWithGoogle,
                            onPressed: () {
                              controller.googleSignIn();
                            },
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
