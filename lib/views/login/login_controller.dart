import 'package:google_sign_in/google_sign_in.dart';
import 'package:authentication/controller/base_controller.dart';
import 'package:authentication/utils/app_constants.dart';
import 'package:authentication/utils/utilitys.dart';
import 'package:authentication/utils/validation_extension.dart';
import 'package:authentication/views/home/home_view.dart';
import 'package:authentication/views/register/register_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:get/get.dart';

class LoginController extends BaseController {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  bool passwordVisible = true;

  void loginApi() {
    if (emailController.text.isEmpty) {
      Utility.showDialog(AppConstants.kPleaseEnterEmailOrUsername);
    } else if (emailController.text.contains("@")) {
      if (emailController.text.isValidEmail() == false) {
        Utility.showDialog(AppConstants.kPleaseEnterValidEmailId);
      } else if (passwordController.text.isEmpty) {
        Utility.showDialog(AppConstants.kPleaseEnterPassword);
      } else {
        Navigator.pushAndRemoveUntil(
            Get.context!,
            MaterialPageRoute(
              builder: (ctx) => HomeView(
                email: emailController.text,
              ),
            ),
            (route) => false);
      }
    } else if (passwordController.text.isEmpty) {
      Utility.showDialog(AppConstants.kPleaseEnterPassword);
    } else {
      Navigator.pushAndRemoveUntil(
          Get.context!,
          MaterialPageRoute(
            builder: (ctx) => HomeView(
              email: emailController.text,
            ),
          ),
          (route) => false);
    }
  }

  Future<void> facebookLogin() async {
    FacebookAuth.instance
        .login(permissions: ["public_profile", "email"]).then((value) {
      FacebookAuth.instance.getUserData().then((userData) async {
        Map userObjec = userData;
        Navigator.push(
          Get.context!,
          MaterialPageRoute(
            builder: (ctx) => RegisterView(
              isSocialLogin: true,
              isGoogleLogin: false,
              user: userObjec,
            ),
          ),
        );
        update();
      });
    });
  }

  Future<void> googleSignIn() async {
    final googleSignIn = GoogleSignIn(
        // you can also use this method for signUp
        // clientId: Platform.isIOS
        //     ? "27671943337-rp4k8qvbcanm90nab97a0t2u8k7duq83.apps.googleusercontent.com"
        //     : "27671943337-9m6uecn2usai07qr25dnftdd7pum7l8p.apps.googleusercontent.com",
        );
    final user = await googleSignIn.signIn();
    await user!.authentication;
    Map<dynamic, dynamic> accountMap = {
      'name': user.displayName,
      'email': user.email,
      'id': user.id,
    };
    Navigator.push(
      Get.context!,
      MaterialPageRoute(
        builder: (ctx) => RegisterView(
          isSocialLogin: true,
          isGoogleLogin: true,
          user: accountMap,
        ),
      ),
    );

    // open second time google accouts options widget if you want to automatically detect last logged google account than comment out this line
    googleSignIn.disconnect();
  }
}
