import 'package:authentication/style/dimensions.dart';
import 'package:authentication/style/text_style.dart';
import 'package:authentication/utils/app_constants.dart';
import 'package:authentication/utils/color.dart';
import 'package:authentication/views/email_varification/email_vari_controller.dart';
import 'package:authentication/views/forgot_password/forgot_controller.dart';
import 'package:authentication/views/home/home_view.dart';
import 'package:authentication/widgets/common_app_button.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class EmailVarificationAView extends GetView<ForgotController> {
  final String email;
  final bool isSocialLogin;
  final bool isGoogleLogin;
  const EmailVarificationAView(
      {super.key,
      this.isSocialLogin = false,
      this.isGoogleLogin = false,
      this.email = ""});

  @override
  Widget build(BuildContext context) {
    return GetBuilder(
      init: EmailVarificationAController(),
      builder: (EmailVarificationAController controller) {
        return SafeArea(
          child: Scaffold(
            appBar: AppBar(
              automaticallyImplyLeading: true,
              title: const Text(
                AppConstants.kEmailVarification,
                style: kAppBarStyle,
              ),
              centerTitle: true,
              backgroundColor: Colors.transparent,
            ),
            body: Center(
              child: Padding(
                padding: const EdgeInsets.all(Dimensions.kPaddingExtraSize),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text.rich(
                      textAlign: TextAlign.center,
                      TextSpan(
                        children: [
                          const TextSpan(
                            text: '${AppConstants.kVarificationMessageFirst} ',
                            style: kInputTextStyle,
                          ),
                          TextSpan(
                            text: email,
                            style: kEmailTextStyle,
                          ),
                          const TextSpan(
                            text: ' ${AppConstants.kVarificationMessageLast}',
                            style: kInputTextStyle,
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(height: 30),
                    SizedBox(
                      width: double.infinity,
                      child: CommonAppButton(
                          backgroundColor: kAppColor,
                          buttonText: AppConstants.kContinue,
                          onPressed: () {
                            Navigator.pushAndRemoveUntil(
                                Get.context!,
                                MaterialPageRoute(
                                  builder: (ctx) => HomeView(
                                    isGoogleLogin: isGoogleLogin,
                                    isSocialLogin: isSocialLogin,
                                    email: email,
                                  ),
                                ),
                                (route) => false);
                          }),
                    )
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
