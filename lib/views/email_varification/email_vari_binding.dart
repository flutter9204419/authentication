import 'package:authentication/views/email_varification/email_vari_controller.dart';
import 'package:get/get.dart';

class EmailVarificationBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<EmailVarificationAController>(
      () => EmailVarificationAController(),
    );
  }
}
