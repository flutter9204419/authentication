import 'package:authentication/controller/base_controller.dart';
import 'package:authentication/utils/app_constants.dart';
import 'package:authentication/utils/utilitys.dart';
import 'package:authentication/utils/validation_extension.dart';
import 'package:authentication/views/email_varification/email_vari_view.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

class RegisterController extends BaseController {
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();
  TextEditingController socialIdController = TextEditingController();
  bool passwordVisible = true;
  bool passwordVisibleR = true;
  bool isChecked = false;

  void signUp() {
    if (nameController.text.isEmpty) {
      Utility.showDialog(AppConstants.kPleaseEnterName);
    } else if (emailController.text.isEmpty) {
      Utility.showDialog(AppConstants.kPleaseEnterEmailId);
    } else if (emailController.text.isValidEmail() == false) {
      Utility.showDialog(AppConstants.kPleaseEnterValidEmailId);
    } else if (passwordController.text.isEmpty) {
      Utility.showDialog(AppConstants.kPleaseEnterPassword);
    } else if (confirmPasswordController.text.isEmpty) {
      Utility.showDialog(AppConstants.kPleaseEnterConfirmPaassword);
    } else if (confirmPasswordController.text != passwordController.text) {
      Utility.showDialog(AppConstants.kConfirmPasswordShouldMatchWithPassword);
    } else if (!isChecked) {
      Utility.showDialog(AppConstants.kPleaseSelectTermsAndConditions);
    } else {
      Navigator.of(Get.context!).push(
        MaterialPageRoute(
          builder: (ctx) => EmailVarificationAView(
            email: emailController.text,
          ),
        ),
      );
    }
  }

  //This methos use for open external browser for showing terms and conditions
  Future<void> launchURL() async {
    if (!await launchUrl(Uri.parse(AppConstants.kUrl))) {
      throw Exception('Could not launch ${AppConstants.kUrl}');
    }
  }

  void socialSignUp(bool isGoogleLogin, bool isSocialLogin) {
    if (socialIdController.text.isEmpty) {
      Utility.showDialog(AppConstants.kPleaseEnterSocialLoginId);
    } else if (nameController.text.isEmpty) {
      Utility.showDialog(AppConstants.kPleaseEnterName);
    } else if (!isChecked) {
      Utility.showDialog(AppConstants.kPleaseSelectTermsAndConditions);
    } else {
      Navigator.push(
          Get.context!,
          MaterialPageRoute(
            builder: (ctx) => EmailVarificationAView(
              isGoogleLogin: isGoogleLogin,
              isSocialLogin: isSocialLogin,
              email: emailController.text,
            ),
          ));
    }
  }
}
