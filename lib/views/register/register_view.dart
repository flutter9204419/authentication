import 'package:authentication/style/dimensions.dart';
import 'package:authentication/style/text_style.dart';
import 'package:authentication/utils/app_constants.dart';
import 'package:authentication/utils/color.dart';
import 'package:authentication/views/login/login_controller.dart';
import 'package:authentication/views/register/register_controller.dart';
import 'package:authentication/widgets/common_app_button.dart';
import 'package:authentication/widgets/text_input_field.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class RegisterView extends GetView<RegisterController> {
  final bool isSocialLogin;
  final bool isGoogleLogin;
  final Map? user;
  const RegisterView(
      {super.key,
      this.isSocialLogin = false,
      this.isGoogleLogin = false,
      this.user});

  @override
  Widget build(BuildContext context) {
    RegisterController controller = Get.put(RegisterController());
    LoginController loginController = Get.put(LoginController());
    return GetBuilder(
      init: RegisterController(),
      initState: (i) {
        if (isSocialLogin) {
          controller.nameController.text = user!["name"];
          controller.emailController.text = user!["email"];
          controller.socialIdController.text = user!["id"];
          controller.isChecked = false;
          controller.passwordController.text = "";
          controller.confirmPasswordController.text = "";
        } else {
          controller.nameController.text = "";
          controller.emailController.text = "";
          controller.socialIdController.text = "";
          controller.isChecked = false;
          controller.passwordController.text = "";
          controller.confirmPasswordController.text = "";
        }
      },
      builder: (RegisterController controller) {
        return WillPopScope(
          onWillPop: () async {
            if (isSocialLogin) {
              controller.nameController.text = "";
              controller.emailController.text = "";
              controller.socialIdController.text = "";
              controller.isChecked = false;
              controller.passwordController.text = "";
              controller.confirmPasswordController.text = "";
              controller.update();
            }
            return true;
          },
          child: GestureDetector(
            onTap: () => FocusScope.of(context).unfocus(),
            child: SafeArea(
              child: Scaffold(
                appBar: AppBar(automaticallyImplyLeading: true),
                body: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.all(Dimensions.kPaddingExtraSize),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          isSocialLogin
                              ? AppConstants.kCompleteProfile
                              : AppConstants.kSignUp,
                          style: kAppLabelStyle,
                        ),
                        const SizedBox(height: 10),
                        if (isSocialLogin) ...[
                          TextInputField(
                            obscureText: false,
                            hintText: AppConstants.kName,
                            keyboardType: TextInputType.text,
                            controller: controller.socialIdController,
                            enable: false,
                          ),
                          const SizedBox(height: 10),
                          TextInputField(
                            obscureText: false,
                            hintText: AppConstants.kName,
                            keyboardType: TextInputType.text,
                            controller: controller.nameController,
                          ),
                          const SizedBox(height: 10),
                          TextInputField(
                            obscureText: false,
                            hintText: AppConstants.kEmail,
                            keyboardType: TextInputType.emailAddress,
                            controller: controller.emailController,
                            enable: false,
                          ),
                          const SizedBox(height: 15),
                        ] else ...[
                          TextInputField(
                            obscureText: false,
                            hintText: AppConstants.kName,
                            keyboardType: TextInputType.text,
                            controller: controller.nameController,
                          ),
                          const SizedBox(height: 10),
                          TextInputField(
                            obscureText: false,
                            hintText: AppConstants.kEmail,
                            keyboardType: TextInputType.emailAddress,
                            controller: controller.emailController,
                          ),
                          const SizedBox(height: 15),
                          TextInputField(
                            obscureText: controller.passwordVisible,
                            hintText: AppConstants.kPassword,
                            keyboardType: TextInputType.name,
                            controller: controller.passwordController,
                            suffixIcon: IconButton(
                              onPressed: () {
                                controller.passwordVisible =
                                    !controller.passwordVisible;
                                controller.update();
                              },
                              icon: Icon(
                                controller.passwordVisible
                                    ? Icons.visibility
                                    : Icons.visibility_off,
                                color: kAppColor,
                              ),
                            ),
                          ),
                          const SizedBox(height: 15),
                          TextInputField(
                            obscureText: controller.passwordVisibleR,
                            hintText: AppConstants.kConfirmPassword,
                            keyboardType: TextInputType.name,
                            controller: controller.confirmPasswordController,
                            suffixIcon: IconButton(
                              onPressed: () {
                                controller.passwordVisibleR =
                                    !controller.passwordVisibleR;
                                controller.update();
                              },
                              icon: Icon(
                                controller.passwordVisibleR
                                    ? Icons.visibility
                                    : Icons.visibility_off,
                                color: kAppColor,
                              ),
                            ),
                          )
                        ],
                        const SizedBox(height: 20),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            SizedBox(
                              width: 24,
                              height: 24,
                              child: Checkbox(
                                value: controller.isChecked,
                                checkColor: kWhite,
                                activeColor: kAppColor,
                                onChanged: (newValue) {
                                  controller.isChecked = !controller.isChecked;
                                  controller.update();
                                },
                              ),
                            ),
                            const Text(
                              "  ${AppConstants.kIAgreeTermCondition} ",
                              style: kTextStyle14,
                            ),
                            CommonTextButton(
                              onPressed: () {
                                // Get.toNamed(Routes.register);
                                controller.launchURL();
                              },
                              child: const Text(
                                AppConstants.kTearmCondition,
                                style: kLintButtonStyle,
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(height: 40),
                        SizedBox(
                          width: double.infinity,
                          child: CommonAppButton(
                            backgroundColor: kAppColor,
                            buttonText: isSocialLogin
                                ? AppConstants.kProceed
                                : AppConstants.kSignUp,
                            onPressed: () {
                              isSocialLogin
                                  ? controller.socialSignUp(
                                      isGoogleLogin, isSocialLogin)
                                  : controller.signUp();
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                bottomNavigationBar: isSocialLogin
                    ? const SizedBox.shrink()
                    : Padding(
                        padding:
                            const EdgeInsets.all(Dimensions.kPaddingExtraSize),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            const Text(AppConstants.kOrSignInWith),
                            const SizedBox(height: 10),
                            Row(
                              children: [
                                Expanded(
                                  child: CommonAppButton(
                                    backgroundColor: kFacebookColor,
                                    buttonText: AppConstants.kLoginWithFacebook,
                                    onPressed: () {
                                      // Get.back();
                                      loginController.facebookLogin();
                                    },
                                  ),
                                ),
                                const SizedBox(
                                  width: 10,
                                ),
                                Expanded(
                                  child: CommonAppButton(
                                    backgroundColor: kGoogleColor,
                                    buttonText: AppConstants.kLoginWithGoogle,
                                    onPressed: () {
                                      // Get.back();
                                      loginController.googleSignIn();
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
              ),
            ),
          ),
        );
      },
    );
  }
}
