import 'dart:developer';

import 'package:authentication/controller/base_controller.dart';
import 'package:authentication/routes/app_routes.dart';
import 'package:authentication/utils/app_constants.dart';
import 'package:authentication/utils/utilitys.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:get/get.dart';
import 'package:google_sign_in/google_sign_in.dart';

class HomeController extends BaseController {
  void showLogoutDialog(bool isGoogleLogin, bool isSocialLogin) {
    Utility.showDialog(
      AppConstants.kLogoutConfirmation,
      () {
        if (isSocialLogin) {
          if (isGoogleLogin) {
            logoutGoogle();
          } else {
            logoutFacebook();
          }
        }
        Get.back();
        Get.offNamedUntil(Routes.login, (route) => false);
      },
      AppConstants.kSignOut,
    );
  }

  void logoutGoogle() async {
    final googleSignin = GoogleSignIn();
    log("Google SignOut");
    await googleSignin.signOut();
  }

  void logoutFacebook() async {
    log("Facebook SignOut");
    await FacebookAuth.instance.logOut();
  }
}
