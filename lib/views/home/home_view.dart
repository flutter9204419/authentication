import 'package:authentication/style/dimensions.dart';
import 'package:authentication/style/text_style.dart';
import 'package:authentication/utils/app_constants.dart';
import 'package:authentication/views/forgot_password/forgot_controller.dart';
import 'package:authentication/views/home/home_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomeView extends GetView<ForgotController> {
  final String? email;
  final bool isSocialLogin;
  final bool isGoogleLogin;
  const HomeView(
      {super.key,
      this.isSocialLogin = false,
      this.isGoogleLogin = false,
      this.email});

  @override
  Widget build(BuildContext context) {
    return GetBuilder(
      init: HomeController(),
      builder: (HomeController controller) {
        return SafeArea(
          child: Scaffold(
            appBar: AppBar(
              title: const Text(AppConstants.kAppBarText, style: kAppBarStyle),
              centerTitle: true,
              backgroundColor: Colors.transparent,
              actions: [
                IconButton(
                    onPressed: () {
                      controller.showLogoutDialog(isGoogleLogin, isSocialLogin);
                    },
                    icon: const Icon(Icons.logout))
              ],
            ),
            body: Center(
              child: Padding(
                padding: const EdgeInsets.all(Dimensions.kPaddingExtraSize),
                child: Text.rich(
                  textAlign: TextAlign.center,
                  TextSpan(
                    children: [
                      const TextSpan(
                        text: '${AppConstants.kWelcomeMessageFirst} ',
                        style: kInputTextStyle,
                      ),
                      TextSpan(
                        text: email,
                        style: kEmailTextStyle,
                      ),
                      const TextSpan(
                        text: ' ${AppConstants.kWelcomeMessageLast}',
                        style: kInputTextStyle,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
