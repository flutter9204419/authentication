import 'package:authentication/style/dimensions.dart';
import 'package:authentication/utils/color.dart';
import 'package:flutter/material.dart';

const kHintTextStyle =
    TextStyle(fontSize: Dimensions.kFontSize16, color: kHintColor);
const kInputTextStyle = TextStyle(
    fontSize: Dimensions.kFontSize16,
    color: kAppColor,
    fontWeight: FontWeight.normal);
const kButtomTextStyle = TextStyle(
  fontSize: Dimensions.kFontSize16,
  color: kWhite,
  fontWeight: FontWeight.normal,
);
const kTextStyle14 = TextStyle(
  fontSize: Dimensions.kFontSize14,
  color: kAppColor,
  fontWeight: FontWeight.normal,
);

const kAppBarStyle = TextStyle(
  fontSize: Dimensions.kFontSize18,
  fontWeight: FontWeight.bold,
);

const kEmailTextStyle = TextStyle(
  color: kLinkColor,
  fontSize: Dimensions.kFontSize16,
);

const kLintButtonStyle = TextStyle(
  decoration: TextDecoration.underline,
  decorationColor: kLinkColor,
  decorationStyle: TextDecorationStyle.solid,
  fontSize: Dimensions.kFontSize14,
  color: kLinkColor,
  fontWeight: FontWeight.normal,
);

final kTextInputBorder = OutlineInputBorder(
  borderRadius: BorderRadius.circular(Dimensions.kTextInputRadius),
  borderSide: const BorderSide(
    color: kTextInputBorderColor,
  ),
);

const kForgotLabelStyle =
    TextStyle(fontSize: Dimensions.kFontSize14, color: kHintColor, height: 1.2);
const kAppLabelStyle = TextStyle(fontSize: Dimensions.kFontSize20);
