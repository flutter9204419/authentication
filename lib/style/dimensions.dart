class Dimensions {
  static const double kFontSize16 = 16;
  static const double kFontSize18 = 18;
  static const double kFontSize14 = 14;
  static const double kFontSize20 = 20;

  static const double kPaddingNormalSize = 16;
  static const double kPaddingSmallSize = 10;
  static const double kPaddingExtraSize = 24;

  static const double kButtonRadius = 30;
  static const double kTextInputRadius = 4;

  static double screenHeight = 0;
  static double screenWidth = 0;
  static int screenLoadTime = 2;
}
