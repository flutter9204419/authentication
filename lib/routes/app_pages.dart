import 'package:authentication/routes/app_routes.dart';
import 'package:authentication/views/email_varification/email_vari_binding.dart';
import 'package:authentication/views/email_varification/email_vari_view.dart';
import 'package:authentication/views/forgot_password/forgot_binding.dart';
import 'package:authentication/views/forgot_password/forgot_view.dart';
import 'package:authentication/views/home/home_binding.dart';
import 'package:authentication/views/home/home_view.dart';
import 'package:authentication/views/login/login_binding.dart';
import 'package:authentication/views/login/login_view.dart';
import 'package:authentication/views/register/register_binding.dart';
import 'package:authentication/views/register/register_view.dart';
import 'package:authentication/views/splash/splash_binding.dart';
import 'package:authentication/views/splash/splash_view.dart';
import 'package:get/get.dart';

class AppPages {
  AppPages._();

  static const initial = Routes.login;

  static final routes = [
    GetPage(
        name: Routes.splash,
        page: () => const SplashView(),
        binding: SplashBinding(),
        transition: Transition.fadeIn),
    GetPage(
        name: Routes.login,
        page: () => const LoginView(),
        binding: LoginBinding(),
        transition: Transition.fadeIn),
    GetPage(
        name: Routes.register,
        // transitionDuration: const Duration(milliseconds: 500),
        page: () => const RegisterView(),
        binding: RegisterBinding(),
        transition: Transition.fadeIn),
    GetPage(
        name: Routes.forgot,
        page: () => const ForgotView(),
        binding: ForgotBinding(),
        transition: Transition.fadeIn),
    GetPage(
        name: Routes.home,
        page: () => const HomeView(),
        binding: HomeBinding(),
        transition: Transition.fadeIn),
    GetPage(
        name: Routes.varification,
        page: () => const EmailVarificationAView(),
        binding: EmailVarificationBinding(),
        transition: Transition.fadeIn),
  ];
}
