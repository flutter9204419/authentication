import 'package:authentication/controller/base_controller.dart';
import 'package:authentication/services/index.dart';

class AppController extends BaseController {
  @override
  void onInit() {
    super.onInit();
    Services().setAppConfig();
  }
}
