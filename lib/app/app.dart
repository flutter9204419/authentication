import 'package:authentication/app/app_controller.dart';
import 'package:authentication/routes/app_pages.dart';
import 'package:authentication/routes/app_routes.dart';
import 'package:authentication/utils/app_constants.dart';
import 'package:authentication/utils/color.dart';
import 'package:authentication/views/login/login_binding.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MyApp extends GetView<AppController> {
  const MyApp({super.key});

  // final AppController _controller = Get.put(AppController(), permanent: true);
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      themeMode: ThemeMode.light,
      theme: ThemeData(
        useMaterial3: true,
        colorScheme: ColorScheme.fromSeed(seedColor: kAppColor,primary: kAppColor),
      ),
      builder: (BuildContext context, Widget? child) {
        final MediaQueryData data = MediaQuery.of(context);
        return MediaQuery(
            data: data.copyWith(textScaleFactor: 1), child: child!);
      },
      initialRoute: Routes.login,
      getPages: AppPages.routes,
      title: AppConstants.kAppName,
      initialBinding: LoginBinding(),
    );
  }
}
